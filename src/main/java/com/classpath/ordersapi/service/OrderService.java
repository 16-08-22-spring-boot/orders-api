package com.classpath.ordersapi.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.model.OrderDto;
import com.classpath.ordersapi.repository.OrderRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {

	private final OrderRepository orderRepository;

	/*
	 * public OrderService(OrderRepository orderRepository) { this.orderRepository =
	 * orderRepository; }
	 */
	@Transactional
	public Order saveOrder(Order order) {
		return this.orderRepository.save(order);
	}
	
	public Map<String, Object> fetchAllOrders(int page, int size, String direction, String property){
		/*
		 * Iterable<Order> itAllOrders = this.orderRepository.findAll(); return
		 * itAllOrders;
		 */
		Sort.Direction sortDirection = direction.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
		PageRequest pageRequest = PageRequest.of(page, size, sortDirection, property);
		
		Page<OrderDto> pageResponse = this.orderRepository.findBy(pageRequest);
		
		long totalPages = pageResponse.getTotalPages();
		int numberOfRecords = pageResponse.getNumberOfElements();
		List<OrderDto> data = pageResponse.getContent();
		long totalRecords = pageResponse.getTotalElements();
		
		Map<String, Object> response = new LinkedHashMap<>();
		
		response.put("records", numberOfRecords);
		response.put("total-records", totalRecords);
		response.put("total-pages", totalPages);
		response.put("data", data);
		
		return response;
	}
	
	@Transactional
	public Order findOrderById(long orderId) {
		return this.orderRepository
							.findById(orderId)
							.orElseThrow(() -> new IllegalArgumentException("invalid order id"));
	}
	
	public void deleteOrderById(long orderId) {
		this.orderRepository.deleteById(orderId);
	}
}
