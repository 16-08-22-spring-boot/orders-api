package com.classpath.ordersapi.util;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;

import com.classpath.ordersapi.model.LineItem;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Configuration
@RequiredArgsConstructor
@Slf4j
@Profile("dev")
//public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent>{
public class BootstrapAppData {

	private final OrderRepository orderRepository;
	private final Faker faker;
	
	@Value("${app.numberOfOrders}")
	private int numberOfOrders;

	//@Override
	@EventListener(classes = ApplicationReadyEvent.class)
	public void onApplicationEvent(ApplicationReadyEvent event) {
		log.info("========================Bootstrapping application data =========================");
		IntStream.range(0, numberOfOrders).forEach(index -> {
			Name name= faker.name();
			//new Order("usha", "vikrant", 44, 32, false, false, true);
			Order order = Order
							.builder()
								.firstName(name.firstName())
								.lastName(name.lastName())
								.email(name.firstName()+"@"+faker.internet().domainName())
								//convert java.util.Date to java.time.LocalDate
								.orderDate(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
								.build();
			
			IntStream.range(0,  faker.number().numberBetween(2, 4)).forEach(value -> {
				LineItem lineItem = LineItem
										.builder()
										.name(faker.commerce().productName())
										.qty(faker.number().numberBetween(2, 4))
										.price(faker.number().randomDouble(2, 400, 600))
										.build();
				order.addLineItem(lineItem);
			});
			double totalOrderPrice = order
										.getLineItems()
										.stream()
										.map(lineItem -> lineItem.getQty() * lineItem.getPrice())
										.reduce(0d, Double::sum);
			order.setPrice(totalOrderPrice);
			//of cascading type, saving the order should also save all the line items by default.
			this.orderRepository.save(order);			
		});
		log.info("========================Bootstrapping application data =========================");
	}
}
