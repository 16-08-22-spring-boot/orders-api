package com.classpath.ordersapi.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.javafaker.Faker;

@Configuration
public class ApplicationConfig {
	
	@Bean
	public User userBean() {
		return new User();
	}
	
	@Bean
	//predicate- returns true|false
	@ConditionalOnProperty(prefix="app", value = "loadUser", havingValue = "true", matchIfMissing = true)
	public User userBeanBasedOnProperty() {
		return new User();
	}
	
	@Bean
	@ConditionalOnBean(name="userBean")
	public User userBeanBasedOnBean() {
		return new User();
	}
	
	@Bean
	@ConditionalOnMissingBean(name="userBean")
	public User userBeanBasedOnMissingBean() {
		return new User();
	}
	
	@Bean
	public Faker faker() {
		return new Faker();
	}

}


class User {
	
}
