package com.classpath.ordersapi.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.service.OrderService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {
	
	private final OrderService orderService;
	
	/*
	 * public OrderRestController(OrderService orderService) { this.orderService =
	 * orderService; }
	 */	
	@GetMapping
	@Operation(description = "fetch all the records",method = "GET")
	public Map<String, Object> fetchAllOrders(
			@RequestParam(name="page", defaultValue = "0", required = false) int page,
			@RequestParam(name="size", defaultValue = "10", required = false) int size,
			@RequestParam(name="sort", defaultValue = "asc", required = false) String direction,
			@RequestParam(name="field", defaultValue = "firstName", required = false) String property){
		
		return this.orderService.fetchAllOrders(page, size, direction, property);
	}
	
	@GetMapping("/{id}")
	public Order findOrderById(@PathVariable long id) {
		return this.orderService.findOrderById(id);
	}
	
	@PostMapping
	@ResponseStatus(code = CREATED)
	public Order saveOrder(@RequestBody @Valid Order order) {
		return this.orderService.saveOrder(order);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code = NO_CONTENT)
	public void deleteOrderById(@PathVariable long id) {
		this.orderService.deleteOrderById(id);	
	}
	

}
