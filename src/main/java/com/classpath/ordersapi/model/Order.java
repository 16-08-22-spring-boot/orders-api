package com.classpath.ordersapi.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
//JPA annotations
@Entity
@Table(name="orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@PastOrPresent(message="order date cannot be in future")
	private LocalDate orderDate;
	
	@Column(name="fname", nullable = false)
	@NotEmpty(message = "first name cannot be empty")
	private String firstName;

	
	@Column(name="lname", nullable = false)
	@NotEmpty(message = "last name cannot be empty")
	private String lastName;
	
	/*
	 * @Password(message="password strength is weak") private String password;
	 */

	@Email(message="email address is not in correct format")
	private String email;
	
	@Min(value=200, message="min order price is 200")
	@Max(value=10000, message="max order price is 10000")
	private double price;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonManagedReference
	private Set<LineItem> lineItems;
	
	/*
	 * add the scaffolding code
	 */
	public void addLineItem(LineItem lineItem) {
		if(this.lineItems == null) {
			this.lineItems = new HashSet();
		}
		this.lineItems.add(lineItem);
		lineItem.setOrder(this);
	}

}
